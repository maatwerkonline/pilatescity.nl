<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


// Constants
define('THEME_NAME', 'Dummy');
define('THEME_VERSION', '1.0.0');
define('TEMPPATH', get_template_directory_uri());
define('IMAGES', TEMPPATH . '/assets/images');


// Check if function theme_by_maatwerkonline_setup() exists
if ( ! function_exists( 'theme_by_maatwerkonline_setup' ) ) {
    
    /** 
     * Sets up theme defaults and registers support for various WordPress features. 
     * Note that this function is hooked into the after_setup_theme hook, which runs before the init hook. 
     * The init hook is too late for some features, such as indicating support for post thumbnails.
     */
    function theme_by_maatwerkonline_setup() {

        /** 
         * Load Theme Textdomain. Make theme available for translation. 
         * 
         * @link https://developer.wordpress.org/reference/functions/load_theme_textdomain/
         */
        load_theme_textdomain( 'maatwerkonline', TEMPPATH . '/languages' );


        /** 
         * Register Nav Menus.
         * 
         * @link https://developer.wordpress.org/themes/functionality/navigation-menus/
         */
        register_nav_menus( array(
            'default' => __('Default Menu', 'maatwerkonline'),
            'topbar' => __('Topbar Menu', 'maatwerkonline'),
            'mobile' => __('Mobile Menu', 'maatwerkonline'),
            'footer' => __('Footer Menu', 'maatwerkonline')
        ) );


        // Add Image Size
        // Would you like to add some Image Sizes? Add the add_image_size( $name, $width, $height, $crop ); right here. Documentation: https://developer.wordpress.org/reference/functions/add_image_size/

        /** 
         * Theme Support.
         * 
         * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/
         */
        include 'inc/theme-support.php';


        // Register Block Styles
        include 'inc/register-block-styles.php';


        // Load the Block Editor Functionalities
        include 'inc/functions-block-editor.php'; 


        // Functions Menu
        include 'inc/functions-menu.php';


        // Additional Custom Post Types
        // Looking for a place to add Additional Custom Post Types? We use the plugin 'MB Custom Post Types & Custom Taxonomies' for this: /wp-admin/edit.php?post_type=mb-post-type


        // Additional Custom Taxonomies
        // Looking for a place to add Additional Custom Taxonomies? We use the plugin 'MB Custom Post Types & Custom Taxonomies' for this: /wp-admin/edit.php?post_type=mb-post-type


        // Additional Fields
        // Looking for a place to add Additional Fields? We use the plugin 'Advance Custom Fields Pro' for this: /wp-admin/edit.php?post_type=acf-field-group

    }
}
add_action( 'after_setup_theme', 'theme_by_maatwerkonline_setup' );


/**
 * Add Styles.
 * 
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_scripts/
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_style/
 */
function theme_styles() {
    // If you would like to use Google Font (https://fonts.google.com/) you can add the font-css here
    // wp_enqueue_style('font-css','https://fonts.googleapis.com/css2?family=Open+Sans:wght@300,600&display=swap');

    // Fetch global styles for editor and front-end with theme.json
    wp_enqueue_style( 'slick-slider', TEMPPATH . '/assets/css/slick.css' );
    wp_enqueue_style( 'global-styles-theme', TEMPPATH . '/assets/css/global-styles.css' );
    wp_enqueue_style( 'bootstrap', TEMPPATH . '/assets/css/bootstrap.min.css', array(), THEME_VERSION );
    wp_enqueue_style( 'theme-style', TEMPPATH . '/style.css', array(), THEME_VERSION );
    wp_enqueue_style( 'theme-animations', TEMPPATH . '/assets/css/animations.css', array(), THEME_VERSION );
    wp_enqueue_style( 'icons-pc', TEMPPATH . '/assets/css/icons/icons-pc.css', array(), THEME_VERSION );
}
add_action('wp_enqueue_scripts', 'theme_styles');


/**
 * Add Scripts.
 * 
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_scripts/
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_script/
 */
function theme_scripts() {
    wp_enqueue_script( 'slick-slider', TEMPPATH . '/assets/js/slick-slider.min.js', array('jquery'), '', false );
    wp_enqueue_script( 'init', TEMPPATH . '/assets/js/init.js', true );
    
    // Only Enqueue Comment Reply Script on frontend if is single, if comments are open and if threaded comments are enabled. @link https://peterwilson.cc/including-wordpress-comment-reply-js/
    if ( is_singular() && comments_open() && get_option('thread_comments') ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'theme_scripts');

/**
 * Register sidebar.
 * 
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 */
require_once __DIR__ . '/inc/sidebars.php';

/**
 * Add the 'Book a class' and 'Login' items to the nav menu
 * 
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 */
function add_alt_menu_to_nav( $items, $args )
{
    $items =   '<div id="alt-menu" class="mobile">
                    <div class="book">
                        <i class="pc pc-arrows"></i> Book a class
                        <ul>
                            <li>
                                <a href="'.get_site_url().'/schedule/#/schedule/site/1">Amsterdam</a>
                            </li>
                            <li>
                                <a href="'.get_site_url().'/schedule/#/schedule/site/2">Rotterdam</a>
                            </li>
                        </ul>
                    </div>
                    <a href="'.get_site_url().'/schedule/#/account" class="login"><i class="pc pc-user"></i>My account</a>
                </div>'.$items;
    return $items;
}
add_filter( 'wp_nav_menu_items', 'add_alt_menu_to_nav', 10, 2 );
?>
