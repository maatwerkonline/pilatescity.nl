<?php
if ( function_exists( 'register_sidebar' ) ) {

	register_sidebar( array (
		'name' => __( 'First Footer Widget', 'maatwerkonline' ),
		'id' => 'first-footer-widget',
		'description' => __( 'First Footer Widget Area', 'maatwerkonline' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	register_sidebar( array (
		'name' => __( 'Second Footer Widget', 'maatwerkonline'),
		'id' => 'second-footer-widget',
		'description' => __( 'Second Footer Widget Area', 'maatwerkonline' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	register_sidebar( array (
		'name' => __( 'Third Footer Widget', 'maatwerkonline' ),
		'id' => 'third-footer-widget',
		'description' => __( 'Third Footer Widget Area', 'maatwerkonline' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	register_sidebar( array (
		'name' => __( 'Fourth Footer Widget', 'maatwerkonline' ),
		'id' => 'fourth-footer-widget',
		'description' => __( 'Fourth Footer Widget Area', 'maatwerkonline' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );
};