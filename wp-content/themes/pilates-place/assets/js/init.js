jQuery(document).ready(function($) {
	"use strict";

	// Smooth Scroll 
	$('a[href*="#"]').not('[href="#"]').not('[href="#!"]').not('[href="#0"]').not('[data-commentid]').click(function(event) { // Remove links that don't actually link to anything
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) { // On-page links
			// Figure out element to scroll to
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			// Does a scroll target exist?
			if (target.length) {
				// Only prevent default if animation is actually gonna happen
				event.preventDefault();
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000, function() {
					// Callback after animation
					// Must change focus!
					var $target = $(target);
					$target.focus();
					if ($target.is(":focus")) { // Checking if the target was focused
						return false;
					} else {
						$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
						$target.focus(); // Set focus again
					};
				});
			}
		}
	});


	// Mobile menu sidebar
	$('#page-content-wrapper').on('click', function() {
		if($('#wrapper').hasClass('toggled')) {
			$('body').toggleClass('menu-open');
			$('#menu-toggle').removeClass('active');
			$('#wrapper').removeClass('toggled');
		};
	}).on('click', '#menu-toggle', function(e) {
		e.stopPropagation();
		if($('#menu-toggle').hasClass('inactive')) {
			$('body').toggleClass('menu-open');
			parent.history.back();
			return false;
		} else {
			$('body').toggleClass('menu-open');
			$(this).toggleClass('active');
			$('#wrapper').toggleClass('toggled');
		}
	});

	// Slick slider
    $('.package-slider').slick({
        autoplay: false,
        infinite: false,
        slidesToShow: 3,
        slidestoScroll: 1,
        arrows: true,
        nextArrow: '<button type="button" class="slick-next"><i class="pc pc-arrows"/></button>',
        prevArrow: '<button type="button" class="slick-prev"><i class="pc pc-arrows"/></button>',
        responsive: [
            {
                breakpoint: 968,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

	// Add a class to the header when the user is scrolled to the top of the page
	$(document).scroll(function() {
		var distFromTop = $(document).scrollTop();
		if(distFromTop <= 0){
			$('.header-container').addClass('top-header');
		} else {
			$('.header-container').removeClass('top-header');
		}
	});

    // Make cenrtain elements on the page the same height
    function matchHeight(elements) {
        var minHeight = 0;

        elements.each(function () {
            if ($(this).height() > minHeight) { minHeight = $(this).height(); }
        });

        elements.height(minHeight);
    }

    $(window).load(function () {
        matchHeight($('.package-name'));
        matchHeight($('.package'));
    });
});
