<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
				</div>
				<footer id="footer">

					<div class="content-width-retainer">

						<div class="footer-banner">
							<img src="<?php echo get_site_url() ?>/wp-content/uploads/2021/12/Logo-Pilates-City-Reformer-Clubs@2x-e1639483770450.png" alt="">
						</div>

						<div class="footer-content container">
							<div class="row footer-contact">
								<div class="col-lg-5">
									<?php dynamic_sidebar(__( 'First Footer Widget', 'maatwerkonline' ));?>
								</div>
								<div class="col-2"></div>
								<div class="col-lg-5">
									<?php dynamic_sidebar(__( 'Second Footer Widget', 'maatwerkonline' ));?>
								</div>
							</div>
							<div class="row footer-links">
								<div class="col-lg-10">
									<div class="footer-links-title">Useful links</div>
									<?php wp_nav_menu( array( 'menu' => 'footer', 'theme_location' => 'footer', 'container_id' => 'footer-menu', 'container_class' => '', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false) ); ?>
								</div>
								<div class="col-lg-2">
									<?php dynamic_sidebar(__( 'Third Footer Widget', 'maatwerkonline' ));?>
								</div>
							</div>
							<div class="row footer-bottom">
								<?php dynamic_sidebar(__( 'Fourth Footer Widget', 'maatwerkonline' ));?>
							</div>
						</div>

						<?php if ( !is_page_template( 'page-leadpage.php' ) ) : ?>
							
						<?php endif; ?>
					</div>

				</footer>

			</div> <!-- end #page-content-wrapper -->

		</div> <!-- end #wrapper -->

		<?php wp_footer(); ?>

	</body>
</html>
