<?php
/**
 * Block Name: Half banner block
 * This is the template that displays the block half-banner-block.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}

$content = get_field('half_banner');

if($content):
	$background = $content['background'];
endif;
?>
 
<div id="<?php echo esc_attr($id); ?>" class="half-banner <?php echo esc_attr($classes); ?>" style="background-image: url('<?php echo $background['url'] ?>');">
	<?php if(have_rows('half_banner')): ?>
		<?php while(have_rows('half_banner')): the_row(); ?>

			<div class="content-width-retainer px-0 container">
				<?php if(have_rows('rows')): ?>
					<?php while(have_rows('rows')): the_row();
						$image = get_sub_field('row_image');
						$image_size = get_sub_field('row_image_size');
						$image_position = get_sub_field('row_image_position');
						$content = get_sub_field('row_content');

						if($image_position == "Left"):
							$row_direction = "row-reverse";
						else:
							$row_direction = "row";
						endif;
						?>
						<div class="row" style="flex-direction: <?php echo $row_direction ?>">
							<div class="col-12 col-md-5"><?php echo $content ?></div>
							<?php if($image_size=="Full width"): ?>
								<div class="col-12 col-md-7 full-width">
									<div style="background-image: url('<?php echo $image['url'] ?>');" class="half-banner-image" alt=""></div>
								</div>
							<?php else: ?>
								<div class="col-12 col-md-7">
									<img src="<?php echo $image['url'] ?>" class="half-banner-image" alt="">
								</div>
							<?php endif; ?>
						</div>
					<?php
					endwhile;
				endif; ?>
			</div>

		<?php
		endwhile;
	endif; ?>
</div>