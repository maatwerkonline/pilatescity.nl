<?php
/**
 * Block Name: Class selector block
 * This is the template that displays the block class-selector-block.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}

$content = get_field('class_selector');

if($content):
	$classes_background = $content['classes_background'];
endif;
?>

<?php
if(have_rows('class_selector')):
	while(have_rows('class_selector')): the_row();?>
 
	<div id="<?php echo esc_attr($id); ?>" class="class-selector <?php echo esc_attr($classes); ?>" style="background-image: url('<?php echo $classes_background['url'] ?>');"> <!-- You may change this element to another element (for example blockquote for quotes) -->
		<div class="class-container">
		<?php if(have_rows('classes')): ?>
			<?php $num = 0; while(have_rows('classes')): the_row();
				$class_name = get_sub_field('class_name');
				?>
			
				<div class="<?php echo "class-" . $num; ?> class">
					<?php echo $class_name; ?>
				</div>

			<?php $num+=1; endwhile; ?>
		<?php endif; ?>
		</div>

		<div class="class-content-container container content-width-retainer">
		<?php if(have_rows('classes')): ?>
			<?php $num = 0; while(have_rows('classes')): the_row(); 
				$class_content = get_sub_field('class_content');
				$class_image = get_sub_field('class_image');
			?>

			<div class="row <?php echo "class-content-" . $num ?> class-content" style="flex-direction: <?php echo $row_direction ?>">
				<div class="col-5">
					<?php echo $class_content ?>
				</div>
				<div class="col-7">
					<div style="background-image: url('<?php echo $class_image['url'] ?>');" class="class-image" alt=""></div>
				</div>
			</div>

			<?php $num+=1; endwhile; ?>
		<?php endif; ?>
		</div>
	</div>

	<?php
endwhile;
endif;
?>