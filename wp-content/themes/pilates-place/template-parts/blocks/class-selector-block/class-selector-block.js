(function($){
	/**
	* initializeBlock
	*
	* Adds custom JavaScript to the block HTML.
	*
	* @param   object $block The block jQuery element.
	* @param   object attributes The block attributes (only available when editing).
	* @return  void
	*/

	var initializeBlockclassselectorblock = function() {
		$(".class-selector").each(function () {
			//give the first category and packages the proper classes to start off
			$(this).find(".class-0").addClass('active');
			$(this).find(".class-content-0").addClass('active');
	
			var id = 0;
	
			//Clicking on a category on top switches what packages are displayed
			$('.class').on('click', function () {
				var temp = $(this).attr('class').split(' ')[0];
				id = temp[temp.length - 1];
				$(this).parent().find('.class.active').removeClass('active');
				$(this).addClass('active');
	
				$(this).parent().parent().find('.class-content-container .class-content.active').removeClass('active');
				$(this).parent().parent().find('.class-content-container .class-content-' + id).addClass('active');
			});
		});
	}

	// Initialize dynamic block preview (editor).
	if( window.acf ) {
		window.acf.addAction( 'render_block_preview/type=class-selector-block', initializeBlockclassselectorblock );
	} else {
		$(document).ready(function(){
			initializeBlockclassselectorblock();
		});
	}
})(jQuery);
				