<?php
/**
 * Block Name: Instructors block
 * This is the template that displays the block instructors-block.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}

$content = get_field('instructors_block');

if($content):
	$title = $content['instructors_title'];
endif;
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?> container"> <!-- You may change this element to another element (for example blockquote for quotes) -->
	<?php if(have_rows('instructors_block')): ?>
		<?php while(have_rows('instructors_block')): the_row();
			$link_amsterdam = get_sub_field('location_link_amsterdam');
			$link_rotterdam = get_sub_field('location_link_rotterdam');
			?>
		
			<?php if(have_rows('instructors')): ?>
				<div class="instructors-container row">
				<?php while(have_rows('instructors')): the_row();
					$image = get_sub_field('instructor_image');
					$name = get_sub_field('instructor_name');
					$functions = get_sub_field('instructor_functions');
					$locations = get_sub_field('instructor_location');
					?>
					<div class="instructor col-12 col-md-6 row color-gray">
						<div class="col-6 instructor-image">
							<img src="<?php echo $image['url'] ?>" alt="">
						</div>
						<div class="col-6 instructor-info">
							<h4><?php echo $name ?></h4>
							<p class="instructor-functions"><?php echo $functions ?></p>
							<?php foreach($locations as $location){ ?>
								<?php
								if($location == "Rotterdam"):
									$link = $link_rotterdam;
								elseif($location == "Amsterdam"):
									$link = $link_amsterdam;
								endif; ?>
								<p class="instructor-location mb-2"><a href="<?php echo $link ?>" class="link"><?php echo $location ?></a></p>
							<?php } ?>
						</div>
					</div>
				<?php endwhile; ?>
				</div>
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
</div>