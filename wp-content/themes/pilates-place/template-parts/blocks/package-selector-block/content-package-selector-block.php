<?php
/**
 * Block Name: Package selector block
 * This is the template that displays the block package-selector-block.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}

$content = get_field('package_selector');

if($content):
	$package_categories = $content['package_categories'];
endif;
?>

<?php
if(have_rows('package_selector')):
	while(have_rows('package_selector')): the_row();?>
 
	<div id="<?php echo esc_attr($id); ?>" class="package-selector package-slider <?php echo esc_attr($classes); ?>"> <!-- You may change this element to another element (for example blockquote for quotes) -->

	<?php
		if(have_rows('packages')):
			while(have_rows('packages')): the_row();
			$package_name = get_sub_field('package_name');
			$nr_of_classes = get_sub_field('number_of_classes');
			$price = get_sub_field('price');
			$link = get_sub_field('link');
			$notice = get_sub_field('notice');
			$nr_of_credits = get_sub_field('number_of_credits');
			?>

			<div class="package" credits="<?php echo $nr_of_credits ?>">
				<h4 class="package-name"><?php echo $package_name ?></h4>

				<div class="package-pricing">
					<span class="package-classes">
						<?php echo $nr_of_classes ?>
						<?php if($nr_of_classes <= 1): ?>
							class
						<?php else: ?>
							classes
						<?php endif; ?>
					</span>
					<span class="package-price-divider">/</span>
					<span class="package-price">€ <?php echo $price ?></span>
				</div>

				<?php if(have_rows('usps')): ?>
					<div class="package-usps">
						<?php while(have_rows('usps')): the_row();?>
						<div class="package-usp <?php if(get_sub_field('special_usp')): echo "special-usp"; endif; ?>">
							<i class="pc pc-check-circle"></i><p><?php echo get_sub_field('usp');  ?></p>
						</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>

				<a class="btn btn-tertiary btn-smaller" href="<?php echo $link ?>" target="_blank">Choose package</a>
					<p class="package-notice"><?php echo $notice; ?></p>
			</div>

			<?php
			endwhile;
		endif;
		?>

	</div>

	<?php
endwhile;
endif;
?>