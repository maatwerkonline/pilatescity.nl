<?php
/**
 * Block Name: Banner block
 * This is the template that displays the block banner-block.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}

$content = get_field('banner_block');
if($content):
	$style = $content['style'];
	$background_image = $content['background_image'];
	$text_image = $content['text_image'];
	$subject_image = $content['subject_image'];
	$text1 = $content['text_1'];
	$text2 = $content['text_2'];
	$link = $content['link'];
endif;

if($background_image):
	$style_background = "background-image: url('" . $background_image['url'] . "')";
endif;
?>

<div id="<?php echo esc_attr($id); ?>" class="banner-block style-<?php echo $style ?> <?php echo esc_attr($classes); ?>"style="<?php echo $style_background ?>"> <!-- You may change this element to another element (for example blockquote for quotes) -->
	<?php
	switch($style):
		case 1:
			?>
			<div class="content-width-retainer banner-content">
				<div class="banner-content-container">
					<?php if($text1): ?>
						<h3 class="banner-text-1 ff-granville"><?php echo $text1 ?></h3>
					<?php endif; ?>
					<?php if($text2): ?>
						<h1 class="banner-text-2 "><?php echo $text2 ?></h1>
					<?php endif; ?>
					<?php if($link): ?>
						<a class="link-primary" href="<?php echo $link['url']?>" target="<?php echo $link['target'] ?>"><?php echo $link['title'] ?></a>
					<?php endif; ?>
				</div>
			</div>
			<?php if($subject_image): ?>
					<img class="banner-subject-image" src="<?php echo $subject_image['url'] ?>" alt="">
				<?php endif; ?>
			<?php
			break;
		case 2:
			?>
			<div class="content-width-retainer banner-content">
				<?php if($text_image): ?>
					<h1 class="banner-text-image"><?php echo $text_image ?></h1>
				<?php endif; ?>
				<div class="banner-content-container">
					<?php if($text1): ?>
						<h4 class="banner-text-1 "><?php echo $text1 ?></h4>
					<?php endif; ?>
					<?php if($text2): ?>
						<p class="banner-text-2 ff-granville"><?php echo $text2 ?></p>
					<?php endif; ?>
					<?php if($link): ?>
						<a class="link-primary" href="<?php echo $link['url']?>" target="<?php echo $link['target'] ?>"><?php echo $link['title'] ?></a>
					<?php endif; ?>
					<?php if($subject_image): ?>
						<img class="banner-subject-image" src="<?php echo $subject_image['url'] ?>" alt="">
					<?php endif; ?>
				</div>
			</div>
			<?php
			break;
		case 3:
			?>
			<div class="content-width-retainer banner-content">
				<div class="banner-content-container">
					<?php if($text1): ?>
						<p class="banner-text-1 "><?php echo $text1 ?></p>
					<?php endif; ?>
					<?php if($text2): ?>
						<p class="banner-text-2 ff-granville"><?php echo $text2 ?></p>
					<?php endif; ?>
					<?php if($link): ?>
						<a class="btn btn-primary" href="<?php echo $link['url']?>" target="<?php echo $link['target'] ?>"><?php echo $link['title'] ?></a>
					<?php endif; ?>
					<?php if($subject_image): ?>
						<img class="banner-subject-image" src="<?php echo $subject_image['url'] ?>" alt="">
					<?php endif; ?>
				</div>
			</div>
			<?php
			break;
		case 4:
			?>
			<div class="content-width-retainer banner-content">
				<?php if($text1): ?>
						<p class="banner-text-1 ff-granville"><?php echo $text1 ?></p>
					<?php endif; ?>
					<?php if($text_image): ?>
						<h1 class="banner-text-image"><?php echo $text_image ?></h1>
					<?php endif; ?>
				<div class="banner-content-container">
					<?php if($link): ?>
						<a class="btn btn-secondary" href="<?php echo $link['url']?>" target="<?php echo $link['target'] ?>"><?php echo $link['title'] ?></a>
					<?php endif; ?>
				</div>
			</div>
			<?php
			break;
		case 5:
			?>
			<div class="content-width-retainer banner-content">
				<?php if($text_image): ?>
					
				<?php endif; ?>
				<div class="banner-content-container">
					<?php if($text1): ?>
						<p class="banner-text-1 ff-granville"><?php echo $text1 ?></p>
					<?php endif; ?>
					<?php if($text2): ?>
						<p class="banner-text-2-back "><?php echo $text2 ?></p>
					<?php endif; ?>
					<?php if($text2): ?>
						<p class="banner-text-2 "><?php echo $text2 ?></p>
					<?php endif; ?>
					<?php if($link): ?>
						<a class="link-primary" href="<?php echo $link['url']?>" target="<?php echo $link['target'] ?>"><?php echo $link['title'] ?></a>
					<?php endif; ?>
					<?php if($subject_image): ?>
						<img class="banner-subject-image" src="<?php echo $subject_image['url'] ?>" alt="">
					<?php endif; ?>
				</div>
			</div>
			<?php
			break;
		case 6:
			?>
			<div class="content-width-retainer banner-content">
				<div class="banner-content-container">
					<?php if($text1): ?>
						<h4 class="banner-text-1"><?php echo $text1 ?></h4>
					<?php endif; ?>
					<?php if($text2): ?>
						<p class="banner-text-2"><?php echo $text2 ?></p>
					<?php endif; ?>
					<?php if($link): ?>
						<a class="btn btn-primary" href="<?php echo $link['url']?>" target="<?php echo $link['target'] ?>"><?php echo $link['title'] ?></a>
					<?php endif; ?>
					<?php if($subject_image): ?>
						<img class="banner-subject-image" src="<?php echo $subject_image['url'] ?>" alt="">
					<?php endif; ?>
				</div>
			</div>
			<?php
			break;
	endswitch;
	?>
</div>