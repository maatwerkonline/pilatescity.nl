<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>

<?php 
$_404_page_settings = get_option( '404-page-setting' );

if( !empty( $_404_page_settings ) ){

	$page = get_post( $_404_page_settings );
	echo apply_filters( 'the_content', $page->post_content );

} else { ?>

	<?php if( current_user_can('administrator') ) : // Check if current user has the rights to go to the Customizer ?>
		<a href="<?php echo admin_url( '/themes.php?page=404-page' ); ?>"><?php _e('Your site currently has no 404 Page. Click here to choose a 404 page to display the content of that page.', 'maatwerkonline'); ?></a> <?php _e('This message is only displayed to administrators', 'maatwerkonline'); ?>
	<?php endif; ?>

<?php }; ?>

<?php get_footer();
