<?php

/**
 * Plugin Name: Maatwerk Online Snippets 
 * Description: Plugins that adds comonly used snippets on your website
 * Author: Maatwerk Online
 * Version: 1.0.0
 * Author URI: http://maatwerkonline.nl/
 */

defined('ABSPATH') or exit; // Exit if accessed directly

class Maatwerk_Online_Snippets {

    /** @var string The plugin version number. */
    var $version = '1.0.0';

    /** @var array Storage for class instances. */
    var $instances = array();

    function __construct() {
        // Do nothing.
    }

    function initialize() {
        // Define constants.
        $this->define('MOS_PLUGIN', __FILE__);
        $this->define('MOS_PATH_URL', plugin_dir_path(__FILE__));
        $this->define('MOS_URL', plugin_dir_url(__FILE__));
        $this->define('MOS_BASENAME', plugin_basename(__FILE__));
        $this->define('MOS_VERSION', $this->version);
        add_action('init', array($this, 'init'));
    }

    // Define global variables 
    function define($name, $value = true) {
        if (!defined($name)) {
            define($name, $value);
        }
    }
    

    // INITIALIZE //
    public function init() {
        add_action('plugins_loaded', array($this, 'mos_load_textdomain'));
        // Plugin functions
        require_once('includes/functions.php');
    }
   
    // Plugin load text Domain
    public function mos_load_textdomain() {
        load_plugin_textdomain('mos', false, dirname(plugin_basename(__FILE__)) . '/languages');
    }

}

// Check if Woocommerce is active 
function activate() {
    global $mos;
    // Fetch plugin code
    $mos = new Maatwerk_Online_Snippets();
    $mos->initialize();
    return $mos;
}

// Instantiate.
activate();

// Run on plugin activation
function install_plugin(){
    $files_plugin = glob(plugin_dir_path(__FILE__).'snippets/*');
    $files_theme = glob(get_template_directory() .'/snippets/*');
    $files = array_merge($files_plugin, $files_theme);
    $options = get_option( 'snippets_name' );
    foreach($files as $file) {
    // Check each file and activate Snippet if Default Active comment is true
        $data = get_file_data($file, array('active'=>'Default Active', 'name' => 'Snippet Name') ); 
        if($data['active'] == 'true' ) {
            if (!empty($options)) {
                if (($key = array_search($file, $options)) !== false) {
                    unset($options[$key]);
                }
            } 
            array_push($options, $file);
        }
    }
    update_option( 'snippets_name', $options );
}
register_activation_hook( __FILE__, 'install_plugin' );