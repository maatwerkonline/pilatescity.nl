<?php

/**
 * Snippet Name: Plugin Dependencies
 * Description: Adds dependencies settings to the plugin overview page
 * Version: 1.0
 * Author: Nick Heurter
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function admin_init_plugin_dependencies() {
    // Add a 'Required' button to all not yet Required plugins
    function dependence_plugins_button() {
        $required_plugins = unserialize( get_option( 'plugin_dependencies_list' ) );

        if( is_maatwerkonline() ){ // If the current user is info@maatwerkonline.nl

            $plugins = get_plugins();
            $plugins = array_keys($plugins);

            if( $required_plugins ){
                $not_required_plugins = array_diff($plugins, $required_plugins);
            } else {
                $not_required_plugins = $plugins;
            };

            // Add the 'disable_dependency_link' link to all Required plugins
            if( $required_plugins ){
                foreach ( $required_plugins as $required_plugin ) {
                    add_action( 'plugin_action_links_' . $required_plugin, 'disable_plugin_dependency_link' );
                };
            };

            // Add the 'enable_dependency_link' link to all Not Required plugins
            foreach ( $not_required_plugins as $not_required_plugin ) {
                add_action( 'plugin_action_links_' . $not_required_plugin, 'enable_plugin_dependency_link' );
            };

        };
    };
    add_action( 'admin_head-plugins.php', 'dependence_plugins_button' );


    // Enable Dependency Link
    function enable_plugin_dependency_link( $links ) {
        $links = array_merge( array(
            '<a class="enable-required" href="#!">' . __( 'Enable Required', 'maatwerkonline' ) . '</a>'
        ), $links );

        return $links;
    };


    // Disable Dependency Link
    function disable_plugin_dependency_link( $links ) {
        $links = array_merge( array(
            '<a class="disable-required" href="#!">' . __( 'Disable Required', 'maatwerkonline' ) . '</a>'
        ), $links );

        return $links;
    };


    // Ajax Update Dependencies List Function
    function update_plugin_dependencies_list(){
        $plugins = unserialize(get_option('plugin_dependencies_list'));

        if( $_POST['add_plugin_path'] ){
            $plugins[] = $_POST['add_plugin_path'];
        } else {
            $remove_plugin_path = $_POST['remove_plugin_path'];
            if ( ( $key = array_search($remove_plugin_path, $plugins ) ) !== false ) {
                unset($plugins[$key]);
            };
        };
        
        update_option( 'plugin_dependencies_list', serialize($plugins) );
        exit();
    };
    add_action( 'wp_ajax_update_plugin_dependencies_list', 'update_plugin_dependencies_list' );


    // jQuery script for saving the data on click trough the update_plugin_dependencies_list() function
    function script_update_plugin_dependencies_list(){
        if( is_maatwerkonline() ){ // If the current user is info@maatwerkonline.nl ?>

            <style type="text/css">.enable-required:before,.disable-required:before {content: '';height: 10px;width: 10px;border-radius: 50%;background-color: #86d641;display: inline-block;margin-right: 7px;}.disable-required:before{background-color: #e2473e;}</style>
            <script type="text/javascript">
                jQuery(".enable-required, .disable-required").on("click", function(){
                    var $this = jQuery(this);
                    var tr = $this.closest("tr");

                    $this
                        .css( "pointer-events", "none" ) // Make sure the button can not be clicked twice
                        .before( "<img class='loader' style='width: 16px;height: 16px;' src='/wp-admin/images/spinner.gif'>" ); // Add the Native WordPress Loader as an indication for the user

                    if($this.hasClass("enable-required")){ // If the 'Enable Required' button is clicked
                        var add_plugin_path = tr.data("plugin");
                    } else { // If the 'Disable Required' button is clicked
                        var remove_plugin_path = tr.data("plugin");
                    };
                    
                    jQuery.ajax({
                        type: "POST",
                        data:{
                            action: "update_plugin_dependencies_list",
                            add_plugin_path: add_plugin_path,
                            remove_plugin_path: remove_plugin_path
                        },
                        url: ajaxurl,
                        success: function(result){
                            $this.prev().remove(); // Remove the Loading icon
                            if($this.hasClass("enable-required")){ // If the 'Enable Required' button was clicked
                                $this
                                    .css("pointer-events", "") // Make sure the button can be clicked again
                                    .text("<?php _e( 'Disable Required', 'maatwerkonline' ); ?>") // Change the text to 'Disable Required'
                                    .addClass("disable-required") // Change the class to 'disable-required'
                                    .removeClass("enable-required"); // Change the class 'inable-required'
                            } else { // If the 'Disable Required' button was clicked
                                $this
                                    .css("pointer-events", "") // Make sure the button can be clicked again
                                    .text("<?php _e( 'Enable Required', 'maatwerkonline' ); ?>") // Change the text to 'Enable Required' 
                                    .addClass("enable-required") // Change the class to 'enable-required'
                                    .removeClass("disable-required"); // Change the class 'disable-required'
                            };
                            console.log(result);
                        },
                        error: function(error){
                            $this.prev().remove(); // Remove the Loading icon
                            console.log(error);
                        }
                    });
                });
            </script>

        <?php };

    };
    add_action( 'admin_print_footer_scripts-plugins.php', 'script_update_plugin_dependencies_list' );


    // jQuery scripts for hidding the 'Deactivate' link for Required Plugins for not info@maatwerkonline.nl users and add a confirmation when clicking the 'Deactivate' link for Required Plugins
    function hide_deactivate_plugin_link() {
        $required_plugins = unserialize( get_option( 'plugin_dependencies_list' ) );
        $selector = array();

        // Walk Trough all Required Plugins
        if( $required_plugins ) {
            foreach ( $required_plugins as $required_plugin ) {
                if ( is_plugin_active($required_plugin) ) {
                    $selector[] = '[data-plugin="'.$required_plugin.'"] a[id^="deactivate"]';
                    $selector_hover_before[] = '[data-plugin="'.$required_plugin.'"] a[id^="deactivate"]:hover:before';
                    $selector_hover_after[] = '[data-plugin="'.$required_plugin.'"] a[id^="deactivate"]:hover:after';
                };
            };
        };

        if( is_maatwerkonline() ){ // If the current user is info@maatwerkonline.nl, hide the 'Deactivate' link for Required Plugins

            // Add a confirmation when clicking the 'Deactivate' link for Required Plugins
            $message = __('Are you sure you would like to deactivate this Plugin? Because this active Theme depence on this Plugin. If you deactivate this plugin functions will break.', 'maatwerkonline');
            if( $selector ){
                $data_plugin_selector = implode(", ", $selector); ?>
                <script type="text/javascript">jQuery(' <?php echo $data_plugin_selector; ?> ').on('click', function(e) {var result = window.confirm('<?php echo $message; ?>'); if (result == false) {e.preventDefault();};});</script>
            <?php };

        } else { // If the current user is not info@maatwerkonline.nl, hide the 'Deactivate' link for Required Plugins

            // Disable the 'Deactivate' link for Required Plugins and show a message
            if( $selector ){
                $data_plugin_selector = implode(", ", $selector);
                $data_plugin_selector_hover_before = implode(", ", $selector_hover_before);
                $data_plugin_selector_hover_after = implode(", ", $selector_hover_after); ?>
                <script type="text/javascript">jQuery(' <?php echo $data_plugin_selector; ?> ').removeAttr('href').prepend('<span class="locked-indicator-icon" aria-hidden="true"></span>');</script>
                <style type="text/css"><?php echo $data_plugin_selector; ?>{position: relative;color: #82878c;cursor: default;}<?php echo $data_plugin_selector_hover_before; ?>{content:"<?php _e('You\'re not allowed to deactive this required plugin.', 'maatwerkonline'); ?>";position: absolute;bottom: -25px;left: -10px;width: max-content;background: #23292d;color: #eee;font-size: 10px;padding: 0 5px;border-radius: 3px;}<?php echo $data_plugin_selector_hover_after; ?>{content: "";position: absolute;bottom: -6px;left: 0;width: 0;height: 0;border-left: 5px solid transparent;border-right: 5px solid transparent;border-bottom: 5px solid #23292d;}.locked-indicator-icon:before {color: #82878c;font-size: 12px;position: relative;top: -1px;margin-right: 5px;}</style>
            <?php };

        };
    };
    add_action( 'admin_print_footer_scripts-plugins.php', 'hide_deactivate_plugin_link' );


    // Add Admin Notices for when a Required Plugin is deactivated
    function admin_notice_theme_plugin_dependencies() {
        $required_plugins = unserialize( get_option( 'plugin_dependencies_list' ) );

        if( $required_plugins ) {

            foreach ( $required_plugins as $required_plugin ) {
                if ( !is_plugin_active( $required_plugin ) ) {
                    $plugin_data = get_file_data(WP_PLUGIN_DIR . '/' . $required_plugin, array('name' => 'Plugin Name'), false);
                    ob_start(); ?>
                        <div class="notice notice-error">
                            <p>
                                <strong><?php printf( __('Your theme depends on the plugin %1$s.', 'maatwerkonline'), $plugin_data['name'] ); ?></strong>
                                <a href="<?php echo wp_nonce_url(admin_url('plugins.php?action=activate&plugin='.$required_plugin), 'activate-plugin_'.$required_plugin); ?>"><?php _e( 'Please click here to activate this plugin.', 'maatwerkonline' ); ?></a>
                            </p>
                        </div>
                    <?php echo ob_get_clean();
                };
            };

        };
    };
    add_action( 'admin_notices', 'admin_notice_theme_plugin_dependencies' );
}
add_action( 'admin_init', 'admin_init_plugin_dependencies' );