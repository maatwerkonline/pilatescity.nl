<?php

/**
 * Snippet Name: Function get_click_count() and set_click_count()
 * Description: Use set_click_count($post_ID) to store the amount of visits on a page or post. Use set click count get_click_count($post_ID) to get the current visits of a post or page.
 * Version: 1.0
 * Author: Nick Heurter
 * Required: false
 * Default Active: false
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function get_click_count( $post_ID, $key ) {
    $count_key = 'post_click_count_' . $key;
    $count = get_post_meta($post_ID, $count_key, true);
    if ($count == '') {
        delete_post_meta($post_ID, $count_key);
        add_post_meta($post_ID, $count_key, '0');
        return '0';
    }
    return $count;
}

function set_click_count($post_ID) {
    $count_key = 'post_click_count_' . $_POST['key'];
    $post_ID = $_POST['post_id'];
    $count = get_post_meta($post_ID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta(get_the_ID(), $count_key);
        add_post_meta($post_ID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($post_ID, $count_key, $count);
    }
    echo $count;

    die();
}