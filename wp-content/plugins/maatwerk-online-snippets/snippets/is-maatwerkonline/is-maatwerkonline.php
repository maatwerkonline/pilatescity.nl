<?php

/**
 * Snippet Name: Function is_maatwerkonline()
 * Description: Use is_maatwerkonline() to check if the current user is an employee of Maatwerk Online
 * Version: 1.0
 * Author: Nick Heurter
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function is_maatwerkonline() {
    $current_user = wp_get_current_user();
    return $current_user->user_email == 'webdevelopment@maatwerkonline.nl';
}
