<?php

/**
 * Snippet Name: Set Page as 404-Page
 * Description: Makes it possible to Set a Page as an 404 Page, so you can edit the 404 page through Pages.
 * Version: 1.0
 * Author: Nick Heurter
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


/**
 * Register Setting for 404 Page
 */
function mo_404_page_register_settings() {

    // Update all settings
    register_setting( '404-page-settings', '404-page-setting' );
}
add_action( 'admin_init', 'mo_404_page_register_settings' );


/**
 * Admin Menu for 404 Page
 */
function mo_404_page_admin_menu() {
    add_theme_page( 
        __('404 Pagina', 'mos'),
        __('404 Pagina', 'mos'),
        'manage_options',
        '404-page',
        'mo_404_page_settings',
    );
}
add_action( 'admin_menu', 'mo_404_page_admin_menu' );


/**
 * Settings for 404 Page
 */
function mo_404_page_settings() { 
    ?>

    <div class="wrap">

        <h1><?php _e('404 Pagina', 'mos'); ?></h1>

        <form action="options.php" method="POST">
   
            <table class="form-table" role="presentation">
                <tbody>
                
                    <?php settings_fields( '404-page-settings' ); ?>
                    <?php do_settings_sections( '404-page-settings' ); ?>
                    <?php $options = get_option( '404-page-setting' ); ?>
                               
                    <tr>

                        <th scope="row">
                            <label for="404_page"><?php _e('404 pagina', 'mos'); ?></label>
                        </th>

                        <td>
                            <select name='404-page-setting' id="404_page">
                                <option value='' <?php if( !empty( $options ) ) { selected( $options, '' ); }; ?>><?php _e('Standaard 404-pagina', 'mos'); ?></option>
                                <?php 
                                $pages = get_pages(); 
                                foreach ( $pages as $page ) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php if( !empty( $options ) ) { selected( $options, $page->ID ); }; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                            <span class="description"><?php printf( __('is %1$sde 404-pagina%2$s.', 'mos'), '<a href="' . esc_url( get_site_url() ) . '/404-error" target="_blank">', '</a>' ); ?></span>
                        </td>
                    
                    </tr>

                </tbody>
            </table>

            <?php submit_button(); ?>

        </form>

    </div>

    <?php
}


/**
 * Change slug to '404-error' slug for Saved 404 Page.
 */
function mo_change_slug_for_404_page( $old_value, $value, $option ){

    $_404_page_setting = get_option( '404-page-setting' );
                        
    if ( !empty( $_404_page_setting ) ) {
        wp_update_post(
            array (
                'ID'        => $_404_page_setting,
                'post_name' => '404-error',
            )
        );
    }

}
add_action('update_option_404-page-setting', 'mo_change_slug_for_404_page', 10, 3);


/**
 * Set Post Status for Saved 404 Page.
 */
function mo_404_page_display_post_states( $post_states, $post ) {
    $_404_page_setting = get_option( '404-page-setting' );
                        
    if ( !empty( $_404_page_setting ) ) { // If there are any custom public post types.
        if( $post->ID == $_404_page_setting ) {
            $post_states[] = __('404 pagina', 'mos');
        }
    }
    return $post_states;
}
add_filter( 'display_post_states', 'mo_404_page_display_post_states', 10, 2 );


/**
 * Fire the below functions when Yoast SEO is active.
 * mo_remove_yoast_seo_metabox_for_404_page()
 * mo_remove_yoast_seo_sidebar_panel_for_404_page()
 */
function mo_yoast_seo_functions_for_page_as_404_page() {
    if ( is_plugin_active( 'wordpress-seo/wp-seo.php' ) || is_plugin_active( 'wordpress-seo-premium/wp-seo-premium.php' ) ) {
        add_action( 'add_meta_boxes', 'mo_remove_yoast_seo_metabox_for_404_page', 11 );
        add_action( 'admin_head-post.php', 'mo_remove_yoast_seo_sidebar_panel_for_404_page' );
        add_action('update_option_404-page-setting', 'mo_noinfex_nofollow_for_404_page', 10, 3);
    }
}
add_action( 'admin_init', 'mo_yoast_seo_functions_for_page_as_404_page' );


/**
 * Remove Yoast SEO Metabox for Saved 404 Page.
 */
function mo_remove_yoast_seo_metabox_for_404_page() {
    global $post;

    $_404_page_setting = get_option( '404-page-setting' );
    if( ! empty( $_404_page_setting ) && $_404_page_setting == $post->ID ) {
        remove_meta_box( 'wpseo_meta', 'page', 'normal' );
    };
}


/**
 * Remove Yoast SEO Sidebar Panel for Saved 404 Page.
 */
function mo_remove_yoast_seo_sidebar_panel_for_404_page(){
    global $post;
    
    $_404_page_setting = get_option( '404-page-setting' );
    if( ! empty( $_404_page_setting ) && $_404_page_setting == $post->ID ) { ?>
        <style>.yoast-seo-sidebar-panel {display: none !important;}</style>
    <?php };
};


/**
 * Set noindex and nofollow to true for Saved 404 Page.
 */
function mo_noinfex_nofollow_for_404_page( $old_value, $value, $option ){

    $_404_page_setting = get_option( '404-page-setting' );
                        
    if ( !empty( $_404_page_setting ) ) {
        update_post_meta( $_404_page_setting, '_yoast_wpseo_meta-robots-noindex', '1' ); // Make sure that for a 404 page the noindex is set to true
        update_post_meta( $_404_page_setting, '_yoast_wpseo_meta-robots-nofollow', '1' ); // Make sure that for a 404 page the nofollow is set to true
    }

}
