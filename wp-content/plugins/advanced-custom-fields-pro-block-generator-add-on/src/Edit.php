<?php
namespace MOB;

class Edit {
	private $post_type;

	public function __construct( $post_type ) {
		$this->post_type = $post_type;

		add_action( 'edit_form_after_title', [ $this, 'output_root' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
	}

	public function output_root() {
		if ( $this->is_screen() ) {
			echo '<div id="root" class="mo-cpt"></div>';
		}
	}

	public function enqueue_scripts() {
		if ( ! $this->is_screen() ) {
			return;
		}

		wp_enqueue_style( $this->post_type, MO_CPT_URL . 'assets/style.css', ['wp-components'], MO_CPT_VER );

		$object = str_replace( 'mo-', '', $this->post_type );
		wp_enqueue_code_editor( ['type' => 'application/x-httpd-php'] );
		wp_enqueue_script( $this->post_type, MO_CPT_URL . "assets/$object.js", ['wp-element', 'wp-components', 'wp-i18n', 'clipboard'], MO_CPT_VER, true );
		wp_localize_script( $this->post_type, 'MOB', $this->js_vars() );
		wp_set_script_translations( $this->post_type, 'acf-block-generator' );
	}

	private function js_vars() {
		$vars = [
			'icons'    => $this->get_icons(),
			'template_direcoty_uri'    => get_template_directory_uri(),
			'settings' => json_decode( get_post()->post_content, ARRAY_A ),
		];

		if ( 'mo-block' === get_current_screen()->id ) {
			$post_types_public = get_post_types( array('public' => true ), 'objects' );
			unset($post_types_public['attachment']);
			$post_type_wp_template_part = get_post_type_object('wp_template_part');
			$post_type_wp_template = get_post_type_object('wp_template');

			$options = [];
			foreach ( $post_types_public as $name => $post_type_public ) {
				$options[ $name ] = $post_type_public->labels->name;
			}
			if($post_type_wp_template_part){
				$options[ $post_type_wp_template_part->name ] = $post_type_wp_template_part->labels->name;
			}
			if($post_type_wp_template){
				$options[ $post_type_wp_template->name ] = $post_type_wp_template->labels->name;
			}
			$vars['post_types'] = $options;
		}

		return $vars;
	}

	private function is_screen() {
		$screen = get_current_screen();
		return 'post' === $screen->base && $this->post_type === $screen->post_type;
	}

	private function get_icons() {
		return [
			'admin-appearance',
			'admin-collapse',
			'admin-comments',
			'admin-generic',
			'admin-home',
			'admin-links',
			'admin-media',
			'admin-network',
			'admin-page',
			'admin-plugins',
			'admin-post',
			'admin-settings',
			'admin-site',
			'admin-tools',
			'admin-users',
			'album',
			'align-center',
			'align-left',
			'align-none',
			'align-right',
			'analytics',
			'archive',
			'arrow-down-alt2',
			'arrow-down-alt',
			'arrow-down',
			'arrow-left-alt2',
			'arrow-left-alt',
			'arrow-left',
			'arrow-right-alt2',
			'arrow-right-alt',
			'arrow-right',
			'arrow-up-alt2',
			'arrow-up-alt',
			'arrow-up',
			'art',
			'awards',
			'backup',
			'book-alt',
			'book',
			'building',
			'businessman',
			'calendar-alt',
			'calendar',
			'camera',
			'carrot',
			'cart',
			'category',
			'chart-area',
			'chart-bar',
			'chart-line',
			'chart-pie',
			'clipboard',
			'clock',
			'cloud',
			'controls-back',
			'controls-forward',
			'controls-pause',
			'controls-play',
			'controls-repeat',
			'controls-skipback',
			'controls-skipforward',
			'controls-volumeoff',
			'controls-volumeon',
			'dashboard',
			'desktop',
			'dismiss',
			'download',
			'editor-aligncenter',
			'editor-alignleft',
			'editor-alignright',
			'editor-bold',
			'editor-break',
			'editor-code',
			'editor-contract',
			'editor-customchar',
			'editor-distractionfree',
			'editor-expand',
			'editor-help',
			'editor-indent',
			'editor-insertmore',
			'editor-italic',
			'editor-justify',
			'editor-kitchensink',
			'editor-ol',
			'editor-outdent',
			'editor-paragraph',
			'editor-paste-text',
			'editor-paste-word',
			'editor-quote',
			'editor-removeformatting',
			'editor-rtl',
			'editor-spellcheck',
			'editor-strikethrough',
			'editor-textcolor',
			'editor-ul',
			'editor-underline',
			'editor-unlink',
			'editor-video',
			'edit',
			'email-alt',
			'email',
			'excerpt-view',
			'exerpt-view',
			'external',
			'facebook-alt',
			'facebook',
			'feedback',
			'flag',
			'format-aside',
			'format-audio',
			'format-chat',
			'format-gallery',
			'format-image',
			'format-links',
			'format-quote',
			'format-standard',
			'format-status',
			'format-video',
			'forms',
			'googleplus',
			'grid-view',
			'groups',
			'hammer',
			'heart',
			'id-alt',
			'id',
			'images-alt2',
			'images-alt',
			'image-crop',
			'image-flip-horizontal',
			'image-flip-vertical',
			'image-rotate-left',
			'image-rotate-right',
			'index-card',
			'info',
			'leftright',
			'lightbulb',
			'list-view',
			'location-alt',
			'location',
			'lock',
			'marker',
			'media-archive',
			'media-audio',
			'media-code',
			'media-default',
			'media-document',
			'media-interactive',
			'media-spreadsheet',
			'media-text',
			'media-video',
			'megaphone',
			'menu',
			'microphone',
			'migrate',
			'minus',
			'money',
			'nametag',
			'networking',
			'no-alt',
			'no',
			'palmtree',
			'performance',
			'phone',
			'playlist-audio',
			'playlist-video',
			'plus-alt',
			'plus',
			'portfolio',
			'post-status',
			'post-trash',
			'pressthis',
			'products',
			'randomize',
			'redo',
			'rss',
			'schedule',
			'screenoptions',
			'search',
			'share1',
			'share-alt2',
			'share-alt',
			'share',
			'shield-alt',
			'shield',
			'slides',
			'smartphone',
			'smiley',
			'sort',
			'sos',
			'star-empty',
			'star-filled',
			'star-half',
			'store',
			'tablet',
			'tagcloud',
			'tag',
			'testimonial',
			'text',
			'tickets-alt',
			'tickets',
			'translation',
			'trash',
			'twitter',
			'undo',
			'universal-access-alt',
			'universal-access',
			'update',
			'upload',
			'vault',
			'video-alt2',
			'video-alt3',
			'video-alt',
			'visibility',
			'welcome-add-page',
			'welcome-comments',
			'welcome-edit-page',
			'welcome-learn-more',
			'welcome-view-site',
			'welcome-widgets-menus',
			'welcome-write-blog',
			'wordpress-alt',
			'wordpress',
		];
	}
}
