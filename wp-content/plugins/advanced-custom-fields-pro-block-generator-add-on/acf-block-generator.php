<?php
/**
 * Plugin Name: Advanced Custom Fields PRO Block Generator Add-On
 * Plugin URI:  https://www.maatwerkonline.nl
 * Description: Create Advanced Custom Fields PRO Blocks with easy-to-use UI
 * Version:     2.1.3
 * Author:      Maatwerk Online
 * Author URI:  https://www.maatwerkonline.nl
 * License:     GPL-2.0+
 * Text Domain: acf-block-generator
 */

// Prevent loading this file directly.
ob_start();
defined( 'ABSPATH' ) || die;
if ( ! function_exists( 'mo_cpt_load' ) ) {
	if ( file_exists( __DIR__ . '/vendor' ) ) {
		require __DIR__ . '/vendor/autoload.php';
	}
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	define( 'MO_CPT_VER', '2.0.6' );
	define( 'MO_CPT_URL', plugin_dir_url( __FILE__ ) );

	add_action( 'init', 'mo_cpt_load', 0 );

	function mo_cpt_load() {
		load_plugin_textdomain( 'acf-block-generator' );

		new MOB\BlockRegister;

		if ( ! is_admin() ) {
			return;
		}

		// Show Meta Box admin menu.
		add_filter( 'rwmo_admin_menu', '__return_true' );

		// Retrieves the ACF version.
		function get_acf_pro_version_for_acf_blocks() {
			// ACF 5 introduces `acf_get_setting`, so this might not always be available.
			if ( function_exists( 'acf_get_setting' ) ) {
				return acf_get_setting( 'version' );
			}

			// Fall back on filter use.
			// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals -- ACF hook.
			return apply_filters( 'acf/get_info', 'version' );
		}

		// Checks if Advanced Custom Fields PRO is installed and active
		if ( !class_exists( 'acf_pro' ) || get_acf_pro_version_for_acf_blocks() > '5.8.0' ) {
			add_action( 'admin_notices', 'acf_pro_for_acf_blocks_message_plugin_not_activated' );
		}

		new MOB\Edit( 'mo-block' );
	}

	// Load the AcfCallback PHP file
	if ( file_exists( __DIR__ . '/src' ) ) {
		require __DIR__ . '/src/AcfCallback.php';
	}

	// Load the BlockImportExport PHP file
	if ( file_exists( __DIR__ . '/src' ) ) {
		require __DIR__ . '/src/BlockImportExport.php';
	}


	/**
	 * Notify that we need ACF to be installed and active.
	 */
	function acf_pro_for_acf_blocks_message_plugin_not_activated() {
		$message = sprintf(
			/* translators: %1$s resolves to Advanced Custom Fields PRO Block Generator Add-On, %2$s resolves to Advanced Custom Fields PRO */
			__( '%1$s requires %2$s PRO 5.8 or higher to be installed and activated.', 'acf-block-generator' ),
			'Advanced Custom Fields PRO Block Generator Add-On',
			'Advanced Custom Fields'
		);

		printf( '<div class="error"><p>%s</p></div>', esc_html( $message ) );
	}

	
	/**
	 * Add the TextDomain of the current theme to the PHP Code field
	 */
	function wpshout_action_example() { 
		$theme_name = wp_get_theme();
	    echo '<input type="hidden" class="act_themes" value="'.esc_html( $theme_name->get( 'TextDomain' ) ).'">'; 
	}
	add_action('admin_footer', 'wpshout_action_example'); 

}